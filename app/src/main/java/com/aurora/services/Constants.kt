package com.aurora.services

object Constants {
    const val BROADCAST_ACTION_INSTALL = "com.aurora.services.ACTION_INSTALL_COMMIT"
    const val BROADCAST_ACTION_UNINSTALL = "com.aurora.services.ACTION_UNINSTALL_COMMIT"
    const val BROADCAST_SENDER_PERMISSION = "android.permission.INSTALL_PACKAGES"
}